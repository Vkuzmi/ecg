#include "nrf_ppi.h"
#include "nrf_drv_saadc.h"
#define SAMPLES_IN_BUFFER 100

void saadc_init(void);
void saadc_sampling_event_init(void);
void saadc_sampling_event_enable(void);
void saadc_sampling_event_disable(void);
extern void saadc_callback(nrf_drv_saadc_evt_t const * p_event);