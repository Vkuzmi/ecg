#include "ad8233.h"
#include "nrf.h"
#include "nrf_drv_gpiote.h"

//Fast Restore Control. Drive FR high to enable fast recovery mode. Otherwise, drive it low.
#define FR      13
//Leads Off Mode Control. Drive the AC/DC pin low for dc leads off mode. Drive the AC/DC pin high for ac leads off mode.
#define AC_DC   14  
//Right Leg Drive Shutdown Control Drive RLD SDN low to power down the RLD amplifier.
#define RLD_SDN         15
//Leads Off Detection Comparator Output.
#define LOD     11
//Operational Amplifier Output. The fully conditioned heart rate signal is present at this output. OUT can be connected to the input of an ADC.
#define OUT     12

uint16_t     m_heart_sygnal_pool[SAMPLES_IN_BUFFER];

void saadc_callback(nrf_drv_saadc_evt_t const * p_event);

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
  if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
  {
    nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
    for (i = 0; i < SAMPLES_IN_BUFFER; i++)
    {
      m_heart_sygnal_pool[i] =  p_event->data.done.p_buffer[i];
    }
    measurement_stop();
  }
}

void measurement_start()
{
  saadc_sampling_event_enable();
}

void measurement_stop()
{
  saadc_sampling_event_disable();
}

void ad8233_samples(uint16_t *samples)
{
  samples = m_heart_sygnal_pool[0];
}


void ad8233_init()
{
  nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(true);
  nrf_drv_gpiote_out_init(FR, &out_config);
  FR = 0;       //disaable fast recovery mode
  nrf_drv_gpiote_out_init(AC_DC, &out_config);
  AC_DC = 0;    //dc leads off mode.
  nrf_drv_gpiote_out_init(RLD_SDN, &out_config);
  RLD_SDN = 0;  //power down the RLD amplifier
  
  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
  in_config.pull = GPIO_PIN_CNF_PULL_Pulldown;
  err_code = nrf_drv_gpiote_in_init(LOD, &in_config, NULL);
  
  saadc_init();
  saadc_sampling_event_init();
}