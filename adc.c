#include "adc.h"

int16_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];

const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(0);
nrf_ppi_channel_t     m_ppi_channel;

void timer_handler(nrf_timer_event_t event_type, void * p_context)
{
  
}



void saadc_sampling_event_init(void)
{
  ret_code_t err_code;
  
  nrf_drv_ppi_init();
  
  nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
  timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
  nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
  
  /* setup m_timer for compare event every ms */
  uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 1);
  nrf_drv_timer_extended_compare(&m_timer,
                                 NRF_TIMER_CC_CHANNEL0,
                                 ticks,
                                 NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                 false);
  nrf_drv_timer_enable(&m_timer);
  
  uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
                                                                              NRF_TIMER_CC_CHANNEL0);
  uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();
  
  /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
  nrf_drv_ppi_channel_alloc(&m_ppi_channel);
  
  nrf_drv_ppi_channel_assign(m_ppi_channel,
                             timer_compare_event_addr,
                             saadc_sample_task_addr);
}


void saadc_sampling_event_enable(void)
{
  nrf_drv_ppi_channel_enable(m_ppi_channel);
}

void saadc_sampling_event_disable(void)
{
  nrf_drv_ppi_channel_disable(m_ppi_channel);
}




void saadc_init(void)
{
  ret_code_t err_code;
  nrf_saadc_channel_config_t channel_config =
  NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);
  
  nrf_drv_saadc_init(NULL, saadc_callback);
  
  nrf_drv_saadc_channel_init(0, &channel_config);
  
  nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
  
  nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
  
}
