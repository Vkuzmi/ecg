#include <stdint.h>
#include "app_uart.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */
#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

void uart_byte_send(uint8_t byte);

void uart_word_send(uint16_t word);

/**@snippet [Handling the data received over UART] */
//void uart_event_handle(app_uart_evt_t * p_event);

/**@snippet [UART Initialization] */
void uart_init(void);
